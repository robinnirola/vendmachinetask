# Vending Machine Project

-- Created Vending Machine Logic using Test Driven Development.
-- I have used Nunit Framework instead MSUnitTest or Xunit Test Framework

IDE Used - Microsoft Visual Studio Professional 2019
Version 16.5.5

 - To run the tests follow the below steps
 - There are two Tests File i.e CoinsTest, ProductTest

 1) Open the Visual Studio 
 2) Load this project
 3) Go to Above two files mentioned CoinsTest, ProductTest
 4) Click on Top two bubles near the Class Name of these file.
 5) Create a New session
 6) It will add to UnitTest Explorer
 7) Just click on green arrove button ( runner button)
 8) It will kick off all the tests of selected file

