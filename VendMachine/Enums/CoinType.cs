﻿
namespace VendMachine.Enums
{
    public enum CoinType
    {
        Nickel,
        Dime,
        Quarter,
        Unacceptable
    }
}
