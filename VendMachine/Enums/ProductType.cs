﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendMachine.Enums
{
    public enum ProductType
    {
        Cola,
        Pepsi,
        Soda
    }
}
