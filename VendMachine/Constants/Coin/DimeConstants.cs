﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendMachine.Constants.Coin
{
    internal static class DimeConstants
    {
        internal const double DimeWeight = 2.268; //gm
        internal const double DimeDiameter = 17.9; //mm
        internal const decimal DimeValue = (decimal)0.10;
    }
}
