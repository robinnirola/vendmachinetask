﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendMachine.Constants.Coin
{
    internal static class NickelConstants
    {
        internal const double NickelWeight = 5.00; //gm
        internal const double NickelDiameter = 21.21; //mm
        internal const decimal NickelValue = (decimal)0.05;
    }
}
