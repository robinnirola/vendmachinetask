﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendMachine.Constants.Coin
{
    internal static class QuarterConstants
    {
        public const double QuarterWeight = 5.670;
        public const double QuarterDiameter = 24.26;
        public const decimal QuarterValue = (decimal)0.25;
    }
}
