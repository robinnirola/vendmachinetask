﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendMachine.Constants.Machine
{
    internal static class DisplayStringConstants
    {
        internal const string DefaultDisplay = "INSERT COIN";
        internal const string ExactChangeOnlyDisplay = "EXACT CHANGE ONLY";
        internal const string ThankYouDisplay = "THANK YOU";
        internal const string PriceDisplayPrefix = "PRICE : $";
        internal const string SoldOutDisplay = "SOLD OUT";
    }
}
