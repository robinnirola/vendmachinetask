﻿
namespace VendMachine.Products
{
    public class Soda : ProductBase
    {
        private const int Stock = 2;
        private const decimal Price = (decimal)0.45;

        public Soda() : base(Stock, Price)
        {

        }
    }
}
