﻿

namespace VendMachine.Products
{
    public interface IProduct
    {
        decimal GetPrice();

        int GetStock();

        void RemoveStock();

        bool Dispense();
    }
}
