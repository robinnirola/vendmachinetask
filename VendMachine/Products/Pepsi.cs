﻿

namespace VendMachine.Products
{
    public class Pepsi : ProductBase
    {
        private const int Stock = 2;
        private const decimal Price = (decimal)0.35;

        public Pepsi() : base(Stock, Price)
        {

        }
    }
}
