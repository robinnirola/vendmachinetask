﻿

namespace VendMachine.Products
{
    public class Cola : ProductBase
    {
        private const int Stock = 2;
        private const decimal Price = (decimal) 0.25;

        public Cola() : base(Stock, Price)
        {

        }
    }
}
