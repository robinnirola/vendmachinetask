﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendMachine.Products
{
    public class SelectedProduct
    {
        public int Stock { get; set; }
        public decimal Price { get; set; }
    }
}
