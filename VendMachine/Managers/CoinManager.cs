﻿using System.Collections.Generic;
using System.Linq;
using VendMachine.Constants.Coin;
using VendMachine.Enums;

namespace VendMachine.Managers
{
    public class CoinManager
    {
        private readonly List<CoinType> _coinBank;

        public CoinManager()
        {
            _coinBank = new List<CoinType>();
        }

        public void AddCoinToBank(CoinType coin)
        {
            _coinBank.Add(coin);
        }

        public CoinType Identify(double weight, double diameter)
        {
            if (weight == NickelConstants.NickelWeight && diameter == NickelConstants.NickelDiameter)
            {
                return CoinType.Nickel;
            }

            if (weight == DimeConstants.DimeWeight && diameter == DimeConstants.DimeDiameter)
            {
                return CoinType.Dime;
            }

            if (weight == QuarterConstants.QuarterWeight && diameter == QuarterConstants.QuarterDiameter)
            {
                return CoinType.Quarter;
            }

            return CoinType.Unacceptable;
        }

        public bool ReturnCoins(decimal currentValue)
        {
            var numberOfQuarters = GetNumberOfQuarters();
            while (currentValue >= QuarterConstants.QuarterValue && numberOfQuarters > 0)
            {
                numberOfQuarters--;
                _coinBank.Remove(CoinType.Quarter);
                currentValue -= QuarterConstants.QuarterValue;
            }

            var numberOfDimes = GetNumberOfDimes();
            while (currentValue >= DimeConstants.DimeValue && numberOfDimes > 0)
            {
                numberOfDimes--;
                _coinBank.Remove(CoinType.Dime);
                currentValue -= DimeConstants.DimeValue;
            }

            var numberOfNickels = GetNumberOfNickels();
            while (currentValue >= NickelConstants.NickelValue && numberOfNickels > 0)
            {
                numberOfNickels--;
                _coinBank.Remove(CoinType.Nickel);
                currentValue -= NickelConstants.NickelValue;
            }

            return currentValue <= 0;
        }

        public bool CheckIfChangeIsAvailable(decimal colaValue, decimal pepsiValue, decimal sodaValue)
        {
            var changeForColaAvailable = CheckIfChangeForProductIsAvailable(colaValue);
            var changeForPepsiAvailable = CheckIfChangeForProductIsAvailable(pepsiValue);
            var changeForSodaAvailable = CheckIfChangeForProductIsAvailable(sodaValue);

            return (changeForSodaAvailable && changeForPepsiAvailable && changeForColaAvailable);
        }

        private bool CheckIfChangeForProductIsAvailable(decimal value)
        {
            var numberOfQuarters = GetNumberOfQuarters();
            while (value >= QuarterConstants.QuarterValue && numberOfQuarters > 0)
            {
                numberOfQuarters--;
                value -= QuarterConstants.QuarterValue;
            }

            var numberOfDimes = GetNumberOfDimes();
            while (value >= DimeConstants.DimeValue && numberOfDimes > 0)
            {
                numberOfDimes--;
                value -= DimeConstants.DimeValue;
            }

            var numberOfNickels = GetNumberOfNickels();
            while (value >= NickelConstants.NickelValue && numberOfNickels > 0)
            {
                numberOfNickels--;
                value -= NickelConstants.NickelValue;
            }

            return value <= 0;
        }

        private int GetNumberOfQuarters()
        {
            var quarters = _coinBank.Where(c => c == CoinType.Quarter).ToList();

            return quarters.Count;
        }

        private int GetNumberOfDimes()
        {
            var dimes = _coinBank.Where(c => c == CoinType.Dime).ToList();

            return dimes.Count;
        }

        private int GetNumberOfNickels()
        {
            var nickels = _coinBank.Where(c => c == CoinType.Nickel).ToList();

            return nickels.Count;
        }

        public decimal GetValueOfBank()
        {
            var quarterValue = GetValueOfQuartersInBank();
            var dimeValue = GetValueOfDimesInBank();
            var nickelValue = GetValueOfNickels();
            return quarterValue + dimeValue + nickelValue;
        }

        private decimal GetValueOfQuartersInBank()
        {
            var quarterCount = GetNumberOfQuarters();

            return quarterCount * QuarterConstants.QuarterValue;
        }

        private decimal GetValueOfDimesInBank()
        {
            var dimeCount = GetNumberOfDimes();

            return dimeCount * DimeConstants.DimeValue;
        }

        private decimal GetValueOfNickels()
        {
            var nickelCount = GetNumberOfNickels();

            return nickelCount * NickelConstants.NickelValue;
        }
    }
}