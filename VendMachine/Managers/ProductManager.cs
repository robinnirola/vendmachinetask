﻿using System;
using VendMachine.Enums;
using VendMachine.Products;

namespace VendMachine.Managers
{
    public class ProductManager
    {
        protected Cola Cola;
        protected Pepsi Pepsi;
        protected Soda Soda;
        protected SelectedProduct SelectedProduct;

        public ProductManager()
        {
            Cola = new Cola();
            Pepsi = new Pepsi();
            Soda = new Soda();
            SelectedProduct = new SelectedProduct();
        }

        public SelectedProduct GetSelectedProductDetails(ProductType productType)
        {
            switch (productType)
            {
                case ProductType.Cola:
                    {
                        SelectedProduct.Price = Cola.GetPrice();
                        SelectedProduct.Stock = Cola.GetStock();
                        return SelectedProduct;
                    }
                case ProductType.Pepsi:
                    {
                        SelectedProduct.Price = Pepsi.GetPrice();
                        SelectedProduct.Stock = Pepsi.GetStock();
                        return SelectedProduct;
                    }
                case ProductType.Soda:
                    {
                        SelectedProduct.Price = Soda.GetPrice();
                        SelectedProduct.Stock = Soda.GetStock();
                        return SelectedProduct;
                    }
                default:
                    return null;
            }
        }

        public void RemoveStock(ProductType productType)
        {
            switch (productType)
            {
                case ProductType.Cola:
                    {
                        Cola.RemoveStock();
                        break;
                    }
                case ProductType.Pepsi:
                    {
                        Pepsi.RemoveStock();
                        break;
                    }
                case ProductType.Soda:
                    {
                        Soda.RemoveStock();
                        break;
                    }
                default:
                    throw new ArgumentOutOfRangeException(nameof(productType), productType, null);
            }
        }

        public bool Dispense(ProductType productType)
        {
            switch (productType)
            {
                case ProductType.Cola:
                    {
                        var colaDispensed = Cola.Dispense();
                        return colaDispensed;
                    }
                case ProductType.Pepsi:
                    {
                        var pepsiDispensed = Pepsi.Dispense();
                        return pepsiDispensed;
                    }
                case ProductType.Soda:
                    {
                        var sodaDispensed = Soda.Dispense();
                        return sodaDispensed;
                    }
                default:
                    return false;
            }
        }
    }
}
