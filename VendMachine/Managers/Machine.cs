﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using VendMachine.Constants.Coin;
using VendMachine.Constants.Machine;
using VendMachine.Enums;
using VendMachine.Products;

namespace VendMachine.Managers
{
    public class Machine
    {
        protected CoinManager CoinManager;
        protected ProductManager ProductManager;
        protected SelectedProduct SelectedProduct;
        public decimal CurrentValue;
        public string CurrentDisplay;

        private bool _productRecentlyDispensed;
        private bool _priceShown;
        private bool _soldOutShown;
        private bool _showCurrentValueNext;


        public Machine()
        {
            CoinManager = new CoinManager();
            ProductManager = new ProductManager();

            CurrentValue = DefaultValueConstants.DefaultValue;
            CurrentDisplay = DisplayStringConstants.DefaultDisplay;

            _productRecentlyDispensed = false;
            _priceShown = false;
            _showCurrentValueNext = false;
            _soldOutShown = false;
        }

        public void InsertCoin(double weight, double diameter)
        {
            var coinType = CoinManager.Identify(weight, diameter);

            var coinAdded = AddOrIgnoreCoinToValue(coinType);

            if (coinAdded)
                CurrentDisplay = CurrentValue.ToString(CultureInfo.InvariantCulture);
        }

        private bool AddOrIgnoreCoinToValue(CoinType coinType)
        {
            if (coinType == CoinType.Unacceptable) return false;
            switch (coinType)
            {
                case CoinType.Dime:
                    CoinManager.AddCoinToBank(coinType);
                    CurrentValue += DimeConstants.DimeValue;
                    return true;
                case CoinType.Nickel:
                    CoinManager.AddCoinToBank(coinType);
                    CurrentValue += NickelConstants.NickelValue;
                    return true;
                case CoinType.Quarter:
                    CoinManager.AddCoinToBank(coinType);
                    CurrentValue += QuarterConstants.QuarterValue;
                    return true;
                case CoinType.Unacceptable:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(coinType), coinType, null);
            }
            return false;
        }

        public void ReturnButtonPressed()
        {
            var coinReturned = CoinManager.ReturnCoins(CurrentValue);

            if (!coinReturned) return;
            CurrentDisplay = DisplayStringConstants.DefaultDisplay;
            CurrentValue = DefaultValueConstants.DefaultValue;
        }

        public void CheckIfChangeIsAvailable()
        {
            var changeAvailable =
                CoinManager.CheckIfChangeIsAvailable(ProductManager.GetSelectedProductDetails(ProductType.Cola).Price,
                    ProductManager.GetSelectedProductDetails(ProductType.Pepsi).Price,
                    ProductManager.GetSelectedProductDetails(ProductType.Soda).Price);

            if (!changeAvailable)
                CurrentDisplay = DisplayStringConstants.ExactChangeOnlyDisplay;

        }

        public void SelectProduct(ProductType product)
        {
            SelectedProduct = ProductManager.GetSelectedProductDetails(product);

            if (SelectedProduct == null) return;
            if (SelectedProduct.Stock > 0)
            {
                _soldOutShown = false;

                if (SelectedProduct.Price <= CurrentValue)
                {
                    var productDispensed = ProductManager.Dispense(product);

                    if (!productDispensed) return;
                    ProductManager.RemoveStock(product);

                    UpdateValuesWhenProductIsDispensed();
                }
                else
                {
                    UpdateDisplayToPrice();
                }
            }
            else
            {
                UpdateStateToSoldOut();
            }
        }
        private void UpdateDisplayToPrice()
        {
            CurrentDisplay = DisplayStringConstants.PriceDisplayPrefix + SelectedProduct.Price;

            _priceShown = true;
        }

        private void UpdateStateToSoldOut()
        {
            CurrentDisplay = DisplayStringConstants.SoldOutDisplay;

            _soldOutShown = true;
        }

        public void CheckDisplay()
        {
            if (_soldOutShown)
            {
                SoldOutState();
            }
            else if (_priceShown)
            {
                if (_showCurrentValueNext)
                {
                    CurrentDisplay = CurrentValue.ToString(CultureInfo.InvariantCulture);

                    _showCurrentValueNext = false;
                }
                else
                {
                    CurrentDisplay = DisplayStringConstants.DefaultDisplay;

                    _showCurrentValueNext = true;
                }
            }
            else if (_productRecentlyDispensed)
            {
                ResetDispenser();
            }
            else
            {
                CurrentDisplay = CurrentValue.ToString(CultureInfo.InvariantCulture);
            }
        }

        private void SoldOutState()
        {
            CurrentDisplay = CurrentValue > DefaultValueConstants.DefaultValue
                    ? CurrentValue.ToString(CultureInfo.InvariantCulture)
                    : DisplayStringConstants.DefaultDisplay;
        }

        private void ResetDispenser()
        {
            CurrentDisplay = DisplayStringConstants.DefaultDisplay;

            CurrentValue = DefaultValueConstants.DefaultValue;

            _productRecentlyDispensed = false;
        }

        private void UpdateValuesWhenProductIsDispensed()
        {
            _productRecentlyDispensed = true;

            _priceShown = false;

            CurrentValue = DefaultValueConstants.DefaultValue;

            CurrentDisplay = DisplayStringConstants.ThankYouDisplay;
        }
    }
}
