﻿using System.Globalization;
using NUnit.Framework;
using VendMachine.Enums;
using VendMachine.Managers;
using VendMachineTests.Helpers;

namespace VendMachineTests
{
    public class CoinTests : TestBase
    {
        protected CoinManager CoinManager;
        protected Machine Machine;

        public CoinTests()
        {
            CoinManager = new CoinManager();
            Machine = new Machine();
        }

        [Test]
        [TestCaseSource(typeof(CoinGenerator))]
        public void WhenValidCoinIsInsertedTheCoinIsRecognized(CoinType insertedCoinType,
            double weight, double diameter, decimal coinValue, bool isValid)
        {
            var coinType = CoinManager.Identify(weight, diameter);

            Assert.AreEqual(coinType, insertedCoinType);
        }


        [Test, NonParallelizable]
        [TestCaseSource(typeof(CoinGenerator))]
        public void WhenValidCoinIsInsertedTheValueIsUpdatedWhenThereAreNoCoinsInsertedBefore(CoinType insertedCoinType,
            double weight, double diameter, decimal coinValue, bool isValid)
        {
            Machine.InsertCoin(weight, diameter);

            if (!isValid)
                Assert.AreNotEqual(coinValue, Machine.CurrentValue);
            else
                Assert.AreEqual(coinValue, Machine.CurrentValue);
        }

        [Test, NonParallelizable]
        [TestCaseSource(typeof(CoinGenerator))]
        public void WhenValidCoinIsInsertedTheValueIsUpdatedWhenThereArePreviousCoinsInserted(CoinType insertedCoinType,
            double weight, double diameter, decimal coinValue, bool isValid)
        {
            Machine.CurrentValue = (decimal) 0.05;

            var oldValue = Machine.CurrentValue;

            Machine.InsertCoin(weight, diameter);

            CheckResultIsEqual(isValid, oldValue + coinValue, oldValue, Machine.CurrentValue);
        }

        [Test]
        public void WhenNoCoinIsInsertedTheMachineShouldDisplayInsertCoins()
        {
            Assert.AreEqual(DefaultDisplay, Machine.CurrentDisplay);
        }

        [Test, NonParallelizable]
        [TestCaseSource(typeof(CoinGenerator))]
        public void WhenValidCoinIsInsertedTheMachineShouldUpdateTheDisplay(CoinType insertedCoinType,
            double weight, double diameter, decimal coinValue, bool isValid)
        {
            Machine.InsertCoin(weight, diameter);

            CheckResultIsEqual(isValid, Machine.CurrentValue.ToString(CultureInfo.InvariantCulture), DefaultDisplay,
                Machine.CurrentDisplay);
        }

        [Test]
        public void WhenReturnCoinsIsPressedTheMoneyIsReturned()
        {
            CoinManager.AddCoinToBank(CoinType.Dime);

            var coinReturned = CoinManager.ReturnCoins(DimeValue);

            Assert.True(coinReturned);
        }

        [Test]
        public void WhenReturnCoinsIsPressedTheMoneyIsDeductedFromTheBank()
        {
            var valueOfBankBefore = CoinManager.GetValueOfBank();
            AddCoinsToBank(CoinType.Quarter, 2);
            AddCoinsToBank(CoinType.Nickel, 2);
            const decimal totalCoinValue = (QuarterValue * 2) + (NickelValue * 2);

            CoinManager.ReturnCoins(totalCoinValue);

            var valueOfBankAfter = CoinManager.GetValueOfBank();
            Assert.AreEqual(valueOfBankBefore, valueOfBankAfter);
        }

        [Test]
        public void WhenReturnCoinsIsPressedTheDisplayIsUpdatedBackToDefault()
        {
            Machine.ReturnButtonPressed();

            Assert.AreEqual(DefaultDisplay, Machine.CurrentDisplay);
        }

        [Test]
        public void WhenTheMachineIsNotAbleToGiveChangeItShouldUpdateTheDisplay()
        {
            Machine.CheckIfChangeIsAvailable();

            Assert.AreEqual(ExactChangeOnlyDisplay, Machine.CurrentDisplay);
        }

        [Test]
        public void WhenTheMachineIsNotAbleToGiveChangeItShouldReturnFalse()
        {
            var changeAvailable = CoinManager.CheckIfChangeIsAvailable(ColaValue, PepsiValue, SodaValue);

            Assert.False(changeAvailable);
        }

        [Test]
        public void WhenTheMachineIsAbleToGiveChangeItShouldReturnTrue()
        {
            AddCoinsToBank(CoinType.Quarter, 10);

            AddCoinsToBank(CoinType.Dime, 10);

            AddCoinsToBank(CoinType.Nickel, 10);

            var changeAvailable = CoinManager.CheckIfChangeIsAvailable(ColaValue, PepsiValue, SodaValue);

            Assert.True(changeAvailable);
        }

        private void AddCoinsToBank(CoinType coin, int numberToAdd)
        {
            for (var i = 0; i < numberToAdd; i++)
            {
                CoinManager.AddCoinToBank(coin);
            }
        }
    }
}