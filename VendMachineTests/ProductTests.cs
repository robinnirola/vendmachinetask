﻿using System.Globalization;
using NUnit.Framework;
using VendMachine.Enums;
using VendMachine.Managers;
using VendMachineTests.Helpers;


namespace VendMachineTests
{
    public class ProductTests : TestBase
    {
        protected Machine Machine;
        protected ProductManager ProductManager;

        public ProductTests()
        {
            Machine = new Machine();
            ProductManager = new ProductManager();
        }

        [Test]
        [TestCaseSource(typeof(ProductSelector))]
        public void WhenAProductIsSelectedAndEnoughMoneyHasBeenInsertedTheDisplaySaysThankYou(ProductType product,
            decimal value)
        {
            SimulateProductSold(product, value);

            Assert.AreEqual(ThankYouDisplay, Machine.CurrentDisplay);
        }

        [Test, NonParallelizable]
        [TestCaseSource(typeof(ProductSelector))]
        public void WhenTheDisplayIsCheckedAndAProductWasRecentlyDispensedTheDisplaySaysInsertCoins(ProductType product,
            decimal value)
        {
            SimulateProductSold(product, value);

            Machine.CheckDisplay();

            Assert.AreEqual(DefaultDisplay, Machine.CurrentDisplay);
        }

        [Test, NonParallelizable]
        [TestCaseSource(typeof(ProductSelector))]
        public void WhenTheDisplayIsCheckedAndAProductWasRecentlyDispensedTheCurrentValueIsReset(ProductType product,
            decimal value)
        {
            SimulateProductSold(product, value);

            Machine.CheckDisplay();

            Assert.AreEqual(DefaultValue, Machine.CurrentValue);
        }

        [Test, NonParallelizable]
        [TestCaseSource(typeof(ProductSelector))]
        public void IfThereIsNotEnoughMoneyForAProductThenTheDisplaySaysPriceAndValue(ProductType product,
            decimal value)
        {
            SimulateSelectingProductNotEnoughCredit(product, value);

            Assert.AreEqual(PriceDisplayPrefix + value, Machine.CurrentDisplay);
        }

        [Test, NonParallelizable]
        [TestCaseSource(typeof(ProductSelector))]
        public void SubsequentDisplayChecksWillDisplayInsertCoinIfThePriceHasBeenDisplayedAlready(ProductType product,
            decimal value)
        {
            SimulateSelectingProductNotEnoughCredit(product, value);

            Machine.CheckDisplay();

            Assert.AreEqual(DefaultDisplay, Machine.CurrentDisplay);
        }

        [Test, NonParallelizable]
        [TestCaseSource(typeof(ProductSelector))]
        public void
            SubsequentDisplayChecksWillDisplayTheCurrentAmountIfThePriceHasBeenDisplayedAndInsertCoinHasBeenDisplayed(
                ProductType product, decimal value)
        {
            SimulateSelectingProductNotEnoughCredit(product, value);
            Machine.CheckDisplay();

            Machine.CheckDisplay();

            Assert.AreEqual(Machine.CurrentValue.ToString(CultureInfo.InvariantCulture), Machine.CurrentDisplay);
        }

        [Test]
        [TestCaseSource(typeof(ProductSelector))]
        public void WhenTheItemSelectedByCustomerIsOutOfStockAndDisplayIsCheckedItWillDisplayInsertCoin(
            ProductType product, decimal value)
        {
            SimulateProductSoldOutState(product, value);

            Machine.CurrentValue = 0;

            Machine.CheckDisplay();

            Assert.AreEqual(DefaultDisplay, Machine.CurrentDisplay);
        }

        [Test]
        [TestCaseSource(typeof(ProductSelector))]
        public void WhenTheItemSelectedByCustomerIsOutOfStockAndDisplayIsCheckedItWillDisplayValueInserted(
            ProductType product, decimal value)
        {
            SimulateProductSoldOutState(product, value);
            Machine.InsertCoin(DimeWeight, DimeDiameter);

            Machine.CheckDisplay();

            Assert.AreEqual(Machine.CurrentValue.ToString(CultureInfo.InvariantCulture), Machine.CurrentDisplay);
        }

        [Test]
        [TestCaseSource(typeof(ProductSelector))]
        public void WhenTheItemSelectedByCustomerIsOutOfStockItWillDisplaySoldOut(ProductType product, decimal value)
        {
            SimulateProductSoldOutState(product, value);

            Assert.AreEqual(SoldOutDisplay, Machine.CurrentDisplay);
        }

        private void SimulateProductSoldOutState(ProductType product, decimal value)
        {
            SimulateProductSold(product, value);
            SimulateProductSold(product, value);
            SimulateProductSold(product, value);
        }

        private void SimulateProductSold(ProductType product, decimal value)
        {
            Machine.CurrentValue = value;
            Machine.SelectProduct(product);
        }

        private void SimulateSelectingProductNotEnoughCredit(ProductType product, decimal value)
        {
            Machine.CurrentValue = DimeValue;
            Machine.SelectProduct(product);
        }
    }
}