﻿using NUnit.Framework;

namespace VendMachineTests
{
    public class TestBase
    {
        #region Machine Constants

        public const string ExactChangeOnlyDisplay = "EXACT CHANGE ONLY";
        public const string DefaultDisplay = "INSERT COIN";
        public const string ThankYouDisplay = "THANK YOU";
        public const string PriceDisplayPrefix = "PRICE : $";
        public const string SoldOutDisplay = "SOLD OUT";
        public const decimal DefaultValue = (decimal) 0.0;

        #endregion

        #region Nickel Constants

        public const double NickelWeight = 5.00;
        public const double NickelDiameter = 21.21;
        public const decimal NickelValue = (decimal) 0.05;

        #endregion

        #region Dime Constants

        public const double DimeWeight = 2.268;
        public const double DimeDiameter = 17.9;
        public const decimal DimeValue = (decimal) 0.10;

        #endregion

        #region Quarter Constants

        public const double QuarterWeight = 5.670;
        public const double QuarterDiameter = 24.26;
        public const decimal QuarterValue = (decimal) 0.25;

        #endregion

        #region Penny Constants

        public const double PennyWeight = 3.11;
        public const double PennyDiameter = 19.05;
        public const decimal PennyValue = (decimal) 0.01;

        #endregion

        #region Product Prices

        public const decimal ColaValue = (decimal) 0.25;
        public const decimal PepsiValue = (decimal) 0.35;
        public const decimal SodaValue = (decimal) 0.45;

        #endregion

        public void CheckResultIsEqual(bool isValid, object valueIfTrue, object valueIfFalse, object value)
        {
            Assert.AreEqual(!isValid ? valueIfFalse : valueIfTrue, value);
        }
    }
}