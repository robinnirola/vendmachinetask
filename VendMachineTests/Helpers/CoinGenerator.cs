﻿using System.Collections;
using System.Collections.Generic;
using VendMachine.Enums;

namespace VendMachineTests.Helpers
{
    internal class CoinGenerator : TestBase, IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[] {CoinType.Nickel, NickelWeight, NickelDiameter,NickelValue, true},
            new object[] {CoinType.Dime, DimeWeight, DimeDiameter, DimeValue,true},
            new object[] {CoinType.Quarter, QuarterWeight, QuarterDiameter,QuarterValue, true},
            new object[] { CoinType.Unacceptable, PennyWeight, PennyDiameter, PennyValue,false}
        };

        public IEnumerator<object[]> GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
