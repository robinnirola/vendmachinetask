﻿using System.Collections;
using System.Collections.Generic;
using VendMachine.Enums;

namespace VendMachineTests.Helpers
{
    public class ProductSelector : TestBase, IEnumerable<object[]>
    {
        private readonly List<object[]> _productTypes = new List<object[]>
        {
            new object[] { ProductType.Cola , ColaValue},
            new object[] { ProductType.Pepsi, PepsiValue},
            new object[] { ProductType.Soda, SodaValue}
        };

        public IEnumerator<object[]> GetEnumerator()
        {
            return _productTypes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
