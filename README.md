-- Created Vending Machine Logic using Test Driven Development.
-- I have used Nunit Framework instead MSUnitTest or Xunit Test Framework
IDE Used - Microsoft Visual Studio Professional 2019
Version 16.5.5

To run the tests follow the below steps
There are two Tests File i.e CoinsTest, ProductTest


Open the Visual Studio
Load this project
Go to Above two files mentioned CoinsTest, ProductTest
Click on Top two bubles near the Class Name of these file.
Create a New session
It will add to UnitTest Explorer
Just click on green arrove button ( runner button)
It will kick off all the tests of selected file

Below are the scenarios :
Write a program to design Vending Machine using your 'favourite language' with all possible tests
Accepts coins of 1,5,10,25 Cents i.e. penny, nickel, dime, and quarter.
Allow user to select products Coke(25), Pepsi(35), Soda(45)
Allow user to take refund by cancelling the request.
Return selected product and remaining change if any.
Allow reset operation for vending machine supplier.